/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import vista.vista;

/**
 * Fichero: leerFichero.java
 * @author Jonatan Recuenco <jonatanrecuenco@hotmail.com>
 * @date 18-feb-2014
 */
public class leerFichero {
    
    leerFichero(){
        File fe = new File ("fichero.csv");
        String s;
        FileReader fr;
        try {
            fr = new FileReader (fe);
            BufferedReader f = new BufferedReader ( fr );
            try {
                while ((s=f.readLine())!=null) {
                    vista.mostrarLinea(s);
                }
                f.close();
            } catch (IOException ex) {
                Logger.getLogger(leerFichero.class.getName()).log(Level.SEVERE, null, ex);
            }
           
        } catch (FileNotFoundException ex) {
            Logger.getLogger(leerFichero.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
