/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;

import java.util.ArrayList;
import java.util.Iterator;
import modelo.cliente;
import util.util;
import vista.vista;
import vista.vistaArrayList;

/**
 * Fichero: ControladorArray.java
 * @author Jonatan Recuenco <jonatanrecuenco@hotmail.com>
 * @date 30-ene-2014
 */
public class controladorArrayList {
    private static int clienteEdad;
    private static String clienteNombre;
    private static String clienteEmail;
    public static ArrayList <String> clientes = new ArrayList <String>();
    
    controladorArrayList() {
            cliente c = new cliente();
            vistaArrayList.getCliente();
            clienteEdad=c.getEdad();
            clienteNombre=c.getNombre();
            clienteEmail=c.getEmail();
            clientes.add(clienteNombre+" "+clienteEdad+" "+clienteEmail);
    }
    
    public static ArrayList getClientes(){
        return clientes;
    }
}