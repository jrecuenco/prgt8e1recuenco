/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import vista.vista;

/**
 * Fichero: escribirFichero.java
 * @author Jonatan Recuenco <jonatanrecuenco@hotmail.com>
 * @date 18-feb-2014
 */
public class escribirFichero {

    escribirFichero(){
        File f = new File ("fichero.csv");
        try {
            FileWriter fw = new FileWriter (f);
            ArrayList clientes = controladorArrayList.getClientes();
            
            for(int i=0;i<clientes.size();i++){
                System.out.println( clientes.get( i ) );
                String cliente = clientes.get(i).toString();
                String [] partes = cliente.split(" ");
                for (int x=0;x<partes.length;x++) {
                    fw.write(partes[x]+";");
                    System.out.println(partes[x]+"\n");
                }
                fw.write("\r\n");
            }
            fw.close();
            vista.escrito();
        } catch (IOException ex) {
            Logger.getLogger(escribirFichero.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
