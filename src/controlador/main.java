/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import modelo.*;
import vista.*;
import util.*;

/**
 * Fichero: main.java
 * @author Jonatan Recuenco <jonatanrecuenco@hotmail.com>
 * Fecha: 15-ene-2014
 */

public class main {
    private static String opcion="";
    
    public static void main(String[] args) {
        while (!opcion.equals("0")){
            opcion = vista.intOpcion();
            cliente c = new cliente();
            while(!opcion.equals("0") && !opcion.equals("1") && !opcion.equals("2") && !opcion.equals("3") && !opcion.equals("4")){
                vista.error(1);
                opcion = vista.intOpcion();
            }
            switch (opcion) {
                case "4":
                    new escribirFichero();
                    break;
                case "3":
                    new leerFichero();
                    break;
                case "2":
                    vistaArrayList.showClientes(controladorArrayList.getClientes());
                    break;
                case "1":
                    new controladorArrayList();
                    break;
                case "0":
                    util.fin();
                    break;
             }    
        }
    }
    
    
    
}