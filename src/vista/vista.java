/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package vista;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import util.errores;

/**
* Fichero: vista.java
* @author Jónatan Recuenco <jonatanrecuenco@hotmail.com>
* @date 30-ene-2014
*/
public class vista {
    static String opcion;
    
    public static String intOpcion() {
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader buffer = new BufferedReader(input);
        try {
            System.out.println("\nESTRUCTURA\n0. Fin\n1. Añadir\n2. Listar\n3. Leer fichero\n4. Grabar fichero");
            opcion = buffer.readLine();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return opcion;
    }
    
    public static void error(int er) {
        String valuePrintEr = errores.errorValues(er);
        System.out.println(valuePrintEr);
    }
    
    public static void escrito() {
        System.out.println("Grabado correctamente en: fichero.csv");
    }
    
    public static void mostrarLinea(String s) {
        System.out.println(s);
    }
    
    public static boolean isNumeric(String cadena){
        try {
		Integer.parseInt(cadena);
		return true;
	} catch (Exception e){
		return false;
	}
    }
}