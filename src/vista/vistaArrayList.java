/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package vista;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import modelo.cliente;
import util.errores;
import static util.util.validar;
import static vista.vista.isNumeric;

/**
* Fichero: vistaAnimal.java
* @author Jónatan Recuenco <jonatanrecuenco@hotmail.com>
* @date 15-nov-2013
*/

public class vistaArrayList {
    
    public static void getCliente() {
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader buffer = new BufferedReader(input);
        BufferedReader buffer2 = new BufferedReader(input);
        BufferedReader buffer3 = new BufferedReader(input);
        try {
            String edad;
            String nombre;
            String email="";
            int edad2=-1;
            String mail ="";
            System.out.println("VISTA ARRAYLIST\nTOMA DE DATOS\nNombre:");
            nombre = buffer.readLine();
            while(edad2<0){
                if (edad2!=-1){
                    vista.error(2);
                }
                System.out.println("Edad:");
                edad = buffer2.readLine();
                if (isNumeric(edad)){
                    edad2 = Integer.parseInt(edad);
                }
                else{
                    edad2 =-2;
                }
            }
            while(mail.equals("")){
                if (email.equals("Error")){
                    vista.error(3);
                }
                System.out.println("Email:");
                mail = buffer3.readLine();
                if (validar(mail)){
                    email = mail;
                }
                else{
                    email = "Error";
                    mail ="";
                }
            }
            cliente.setNombre(nombre);
            cliente.setEmail(email);
            cliente.setEdad(edad2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showClientes(ArrayList arrayList) {
        System.out.println("VISTA ARRAYLIST\nMOSTRANDO DATOS\n");
        Iterator iter = arrayList.iterator();
        while (iter.hasNext()) {
            System.out.println(iter.next());
        }
    }

}