/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package util;

/**
* Fichero: errores.java
* @author Jónatan Recuenco <jonatanrecuenco@hotmail.com>
* @date 15-nov-2013
*/
public class errores {
    public static String errorString;
    public static String errorValues(int er){
        switch (er){
            case 1:
                errorString="Error: Opcion incorrecta del menu";
                break;
            case 2:
                errorString="Error: El valor debe ser un número positivo en la edad";
                break;
            case 3:
                errorString="Error: Email incorrecto";
                break; 
        }
        return errorString;
    }
}
