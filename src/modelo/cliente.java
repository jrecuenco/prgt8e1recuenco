/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modelo;

/**
* Fichero: cliente.java
* @author Jónatan Recuenco <jonatanrecuenco@hotmail.com>
* @date 02-feb-2014
*/
public class cliente {
    static int edad;
    static String nombre;
    static String email;
    
    public cliente() {
        edad = 0;
        nombre = "";
        email = "";
    }
    
    public static void setEdad(int e){
        edad = e;
    }
    
    public int getEdad(){
        return edad;
    }
    
    public static void setNombre(String n){
        nombre = n;
    }
    
    public String getNombre(){
        return nombre;
    }
    
    public static void setEmail(String n){
        email = n;
    }
    
    public String getEmail(){
        return email;
    }
}
